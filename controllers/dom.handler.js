(function( globalContext ) {
    "use strict";

        const defaultDelay = 1500;
        let videosMap = new Map();

        class VideoSearcher
        {
            constructor() {}

            start() {
                if(this.intervalId === undefined)
                    this.intervalId = globalContext.setInterval( () => this.process(), defaultDelay);
            }

            stop() {
                if(this.intervalId !== undefined) {
                    clearInterval(this.intervalId);
                    videosMap.clear();
                }
            }

            process() {
                try {
                    this.deleteNotExisted();
                    this.findVideos(document);
                    this.populateFromIframes(document);

                    this.logVideosInfo();
                }
                catch (exception) {
                    console.log(exception);
                }
            }
            
            findVideos( htmlElement ) {
                let items = htmlElement.getElementsByTagName("video");

                if (items.length === 0)
                    return;

                for (let i = 0; i < items.length; i++) {
                    let currentVideo = items[i];

                    if(!videosMap.has(currentVideo))
                        videosMap.set(currentVideo, new VideoParamsHandler(currentVideo));
                }            
            }

            populateFromIframes( htmlElement ) {
                let iframes = htmlElement.getElementsByTagName('iframe');
                
                if (iframes.length === 0)
                    return;

                for (let i = 0; i < iframes.length; i++) {
                    let iframe = iframes[i];
                    let innerDoc;

                    try {
                        innerDoc = iframe.contentDocument || iframe.contentWindow.document;
                    }
                    catch (exception) {
                        console.log(exception.message);
                    }

                    if (innerDoc !== undefined) {
                        this.findVideos(innerDoc);
                        this.populateFromIframes(innerDoc);
                    }
                }
            }

            deleteNotExisted() {
                if(videosMap == null || videosMap.size == 0)
                    return;

                videosMap.forEach( (value, key) => {
                    if (!this.isInDocument( document, key) && !this.isInIframes( document, key))
                        videosMap.delete(key);
                });
            }

            isInDocument( htmlDocument, video ) {
                let result = htmlDocument.body.contains(video);

                return result;
            }

            isInIframes( htmlElement, video ) {
                let iframes = htmlElement.getElementsByTagName('iframe');

                if (iframes.length === 0)
                    return false;

                for (let i = 0; i < iframes.length; i++) {
                    let iframe = iframes[i];
                    let innerDoc;

                    try {
                        innerDoc = iframe.contentDocument || iframe.contentWindow.document;
                    }
                    catch (exception) {
                        console.log(exception.message);
                    }

                    if (innerDoc !== undefined) {
                        if (this.isInDocument( innerDoc, video) || this.isInIframes( innerDoc, video))
                            return true;
                    }
                }

                return false;
            }

            logVideosInfo() {
                if (videosMap === null || videosMap.size === 0) {
                    console.log("Videos not found.");
                    return;
                }
                
                console.log(`Found ${videosMap.size} videos in the page.`);

                videosMap.forEach( (value, key) => {
                    let logString = this.constructLogInfo(value);
                    console.log(logString);
                });
            }

            constructLogInfo( videoElement ) {
                if (videoElement instanceof VideoParamsHandler) 
                    return `Url: ${videoElement.videoUrl}; Duration: ${videoElement.duration}; 
                            Current time: ${videoElement.currentTime}; Playing time: ${videoElement.playTime}`;
                else
                    return  "Error: given element is not instance of VideoParamsHandler";
            }
        }

        class VideoParamsHandler
        {
            constructor( videoNode ) {
                if (videoNode.tagName !== "VIDEO")
                    throw "given element is not a video";

                this.videoNode = videoNode;
                this.playTime = 0;

                this.setEvents();
            }

            get currentTime() {
                return this.videoNode.currentTime;
            }

            get videoUrl() {
                return this.videoNode.src === "" ? "url is not set" : this.videoNode.src;
            }

            get duration() {
                return Number.isNaN(this.videoNode.duration) ? 0 : this.videoNode.duration;
            }

            setEvents() {
                this.videoNode.addEventListener('pause', this.handlePausedEvent.bind(this), false);
                this.videoNode.addEventListener('play', this.handlePlayEvent.bind(this), false);
            }

            handlePausedEvent( event ) {
                console.log(`The video ${this.videoUrl} is paused on ${this.currentTime}`);

                if(this.intervalId !== undefined)
                    clearInterval(this.intervalId);
            }

            handlePlayEvent( event ) {
                console.log(`The video ${this.videoUrl} start playing on ${this.currentTime}`);

                this.intervalId = globalContext.setInterval( () => { this.playTime++ }, 1000);
            }
        }

        let searcher = new VideoSearcher();
        searcher.start();
})( window );